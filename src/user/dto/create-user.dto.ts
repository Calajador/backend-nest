import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator';
import { UserRole } from '../entities/user.entity';

export class UserDto {
  @IsString()
  @MinLength(4)
  @MaxLength(10)
  username?: string;

  @IsString()
  name?: string;

  @IsEmail()
  email?: string;

  @IsString()
  @MinLength(6)
  @MaxLength(10)
  password?: string;

  role?: UserRole;
}
