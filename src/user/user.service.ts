import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  catchError,
  elementAt,
  from,
  map,
  Observable,
  switchMap,
  throwError,
} from 'rxjs';
import { AuthService } from 'src/auth/auth.service';
import { Repository } from 'typeorm';
import { UserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity, UserRole } from './entities/user.entity';
import {
  paginate,
  Pagination,
  IPaginationOptions,
} from 'nestjs-typeorm-paginate';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private auth: AuthService,
  ) {}
  create(user: UserDto): Observable<UserDto> {
    return this.auth.hashPassword(user.password).pipe(
      switchMap((passwordHash: string) => {
        const newUser = new UserEntity();
        newUser.name = user.name;
        newUser.username = user.username;
        newUser.email = user.email;
        newUser.password = passwordHash;
        newUser.role = UserRole.USER;

        return from(this.userRepository.save(newUser)).pipe(
          map((user: UserDto) => {
            const { password, ...result } = user;
            return result;
          }),
          catchError((err) => throwError(err)),
        );
      }),
    );
  }

  findAll(): Observable<UserDto[]> {
    return from(this.userRepository.find()).pipe(
      map((users: UserDto[]) => {
        users.forEach((element) => {
          delete element.password;
        });
        return users;
      }),
    );
  }

  pageReturn(options: IPaginationOptions): Observable<Pagination<UserDto>> {
    return from(paginate<UserDto>(this.userRepository, options)).pipe(
      map((userPageable: Pagination<UserDto>) => {
        userPageable.items.forEach((element) => {
          delete element.password;
        });
        return userPageable;
      }),
    );
  }

  findOne(id: number) {
    return from(this.userRepository.findOne({ id })).pipe(
      map((user: UserDto) => {
        const { password, ...result } = user;
        return result;
      }),
    );
  }

  update(id: number, user: UpdateUserDto): Observable<any> {
    delete user.email;
    delete user.password;
    delete user.role;
    return from(this.userRepository.update(id, user));
  }

  remove(id: number): Observable<any> {
    return from(this.userRepository.delete(id));
  }

  login(user: UserDto): Observable<string> {
    return this.validateUser(user.email, user.password).pipe(
      switchMap((user: UserDto) => {
        if (user) {
          return this.auth.generateJWT(user).pipe(map((jwt: string) => jwt));
        } else {
          return 'Wrong Credentials';
        }
      }),
    );
  }

  validateUser(email: string, password: string): Observable<UserDto> {
    return this.findByMail(email).pipe(
      switchMap((user: UserDto) =>
        this.auth.comparePasswords(password, user.password).pipe(
          map((match: boolean) => {
            if (match) {
              const { password, ...result } = user;
              return result;
            } else {
              throw Error;
            }
          }),
        ),
      ),
    );
  }

  findByMail(email: string): Observable<UserDto> {
    return from(this.userRepository.findOne({ email }));
  }

  updateRoleofuser(id: number, user: UserDto): Observable<any> {
    return from(this.userRepository.update(id, user));
  }
}
